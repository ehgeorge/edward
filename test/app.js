let express = require('express');
let mysql = require('mysql');
let Promise = require('promise');
let logger = require('morgan');

let app = express();

app.use(logger('dev'));

let http = require('http');
let port = parseInt(process.env.PORT || '3000',10);
app.set('port', port);
let server = http.createServer(app);
server.listen(port);


//let assert = require('assert');
let pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'Temp',
    database: 'ichor'
});
let all = {};


all.select = function () {
    return new Promise(function (fulfill, reject) {
        pool.getConnection(function (error_outer, connection) {
            if(error_outer){
                reject(error_outer);
            }
            else {
                connection.query('SELECT * FROM user', function (error_inner, results, fields) {
                    connection.destroy();
                    if (error_inner) reject(error_inner);
                    else fulfill(results);
                });
            }

        });

    });
};

all.insert = function (name,pass,credit,priority) {
    return new Promise(function (fulfill, reject) {
        pool.getConnection(function (error_outer, connection) {
            if(error_outer){
                reject(error_outer);
            }
            else {
                connection.query("INSERT INTO user (username, password, credits, priority) VALUES (?,?,?,?);", [name, pass, credit,priority],  function (error_inner, results, fields) {
                    connection.destroy();
                    if (error_inner) reject(error_inner);
                    else fulfill();
                });
            }

        });

    });
};

all.remove = function(){
    return new Promise(function (fulfill, reject) {
        pool.getConnection(function (error_outer, connection) {
            if(error_outer){
                reject(error_outer);
            }
            else {
                connection.query('DELETE FROM user', function (error_inner, results, fields) {
                    connection.destroy();
                    if (error_inner) reject(error_inner);
                    else fulfill();
                });
            }

        });

    });
};

all.updatePriority = function(username, new_priority){
    return new Promise(function(fulfill, reject) {
        pool.getConnection(function (error_outer, connection){
            if (error_outer){
                reject(error_outer)
            }
            else{
                connection.query('UPDATE user SET priority = ? WHERE username = ?;',[new_priority, username], function (error_inner){
                    connection.destroy();
                    if (error_inner) reject(error_inner);
                    else fulfill();
                });
            }
        });
    });
};

/*Not too sure how to do use the promises in this bit*/

app.use('/insert',function(req,res,next){
    let name = req.query.n;
    let pass = req.query.p;
    let cred = req.query.c;
    let priority = req.query.priority;
    all.insert(name,pass,cred,priority).then(function() {
        next();
    }).catch(function(error){
        console.log(error);
        res.setHeader('Content-Type', 'application/json');
        res.send({error:error.code})
    });
});

app.use('/remove',function(req,res,next){
    let promise1 = all.remove();
    promise1.done(function() {
        next();
    });
    promise1.catch(function(error){
        console.log(error);
        next();
    });
});

app.use('/update', function(req,res,next){
    let name = req.query.name;
    let priority = req.query.pri;
    let promise1 = all.updatePriority(name,priority);
    promise1.done(function(){
        next();
    });
    promise1.catch(function(error){
        console.log(error);
        res.setHeader('Content-Type', 'application/json');
        res.send({error:error.code});
    })

});

app.use('/',function(req, res, next){
    res.setHeader('Content-Type', 'application/json');
    all.select().then(function(results){
        res.send(results);
    }).catch(function(error){
        console.log(error);
    });
});



module.exports = all;