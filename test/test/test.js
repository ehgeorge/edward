let assert = require('assert');
let all = require('../app');


describe('assertion tests',function () {
    this.slow(50);
    this.timeout(150);

    it('Empty Test',function(done) {
        all.remove().then(function() {
            all.select().then(function (results) {
                done(assert.equal(results.length, 0));
            });
        });
    });




    it('Deletion Test',function(done) {
        all.insert("ward", "password", 50,1).then( function () {
            all.remove().then(function () {
                all.select().then(function (results) {
                    done(assert.equal(results.length, 0));
                });
            });
        });
    });


    it('Insertion Test',function(done){
        all.insert("edward","password",50,1).then(function(){
            all.select().then(function(results){
                let result = results[0];
                done(assert.equal(result.username,"edward"));
                all.remove();
            });
        });
    });


    it('Update Test',function(done){
        all.insert("edward","password",50,1).then(function(){
            all.updatePriority("edward",2).then(function(results){
                all.select().then(function(results) {
                    let result = results[0];
                    done(assert.equal(result.priority, 'Medium'));
                    all.remove();
                });
            });
        });
    })


});
