#Assertion Tests

---

###How to run the tests

Ensure the packages are installed with `npm install`

To run the tests, in the terminal type `npm test`.

***

#How to use the server

---

* Go to the website [here](http://176.58.122.124:3000)
* To insert a user type /insert?n='username'&p='password'&c='number of credits'. for example: <http://176.58.122.124:3000/insert?n=jack&p=pass&c=5>
* To clear all the results go to /remove


#How to set up the sql server:

---

1. Make sure the execsql is installed globally

    `npm install -g execsql`

2. Configure the db access

    `execsql -c "localhost" "root" "root"`

3. Execute the .sql file

    `execsql -f test/db.sql`
    



