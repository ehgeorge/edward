let execsql = require('execsql'),
    dbConfig = {
        host: 'localhost',
        user: 'root',
        password: ''
    },
    sql = 'use ichor_second;',
    sqlFile = 'db.sql';
execsql.config(dbConfig)
    .exec(sql)
    .execFile(sqlFile, function(err, results){
        console.log(results);
    }).end();