CREATE TABLE `user` (
	`username` varchar(50) NOT NULL,
	`password` varchar(50) NOT NULL,
	`credits` INT NOT NULL,
	`priority` ENUM('Low','Medium','High') NOT NULL,
	PRIMARY KEY (`username`)
);

